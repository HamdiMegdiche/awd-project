# AWD Project : Hexagone Team
Esprit enter-aide project with micro-services architecture. 
### Specification :
The main project contains 6 modules (open it with intelliJ IDEA IDE):

**Api distribution (with zuul proxy):**
* [Eureka Server](http://localhost:8000/) : Discovery server for spring boot micro services clients.
* [Carpooling](http://localhost:8000/api/carpooling/all) : 
Micro-service built with Spring boot & embedded H2 database (assignee Skander).
* [Flatsharing](http://localhost:8000/api/flatsharing/all) : 
Micro-service built with Spring boot & embedded H2 database (assignee Idriss).
* [Lost & found items](http://localhost:8000/api/lostfound/all) :
Micro-service built with Spring boot & embedded H2 database (assignee Monstassar).
* [Memes (photos)](http://localhost:8000/api/memes/all): 
Micro-service built with Spring boot & embedded H2 database (assignee Hamdi).
* [User micro-service](http://localhost:8000/api/user):
Micro-service built with LoopBack 3 & remote Mongo database (assignee Hamdi).
* [User explorer with swagger](http://localhost:8005/explorer/)

### Installation :
For the User micro-service 
* [Project readme](https://gitlab.com/HamdiMegdiche/awd-project/tree/master/user-service)

For other modules :
* Run the eureka server micro-service along with any other micro-service (or all at once).

