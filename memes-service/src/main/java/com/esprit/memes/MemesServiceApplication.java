package com.esprit.memes;

import com.esprit.memes.config.FileStorageProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
@EnableConfigurationProperties({FileStorageProperties.class})
public class MemesServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(MemesServiceApplication.class, args);
    }

}
