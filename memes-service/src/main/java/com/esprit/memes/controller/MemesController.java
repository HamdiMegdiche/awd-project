package com.esprit.memes.controller;


import com.esprit.memes.model.File;
import com.esprit.memes.repository.FileRepository;
import com.esprit.memes.service.FileStorageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class MemesController {

    @Autowired
    private FileRepository fileRepository;
    @Autowired
    private FileStorageService fileStorageService;

    @GetMapping("/{id}")
    public ResponseEntity getById(@PathVariable("id") final Integer id) {
        Optional<File> f = fileRepository.findById(id);
        return f.map(file -> new ResponseEntity<>(file, HttpStatus.OK))
                .orElseGet(() -> new ResponseEntity<>(HttpStatus.NOT_FOUND));
    }


    @GetMapping("/all")
    public List<File> getAll() {
        return fileRepository.findAll();
    }

    @PostMapping("/add")
    public File newMeme(@RequestParam("file") MultipartFile file, @RequestParam String legend, @RequestParam String userId) {
        String fileName = fileStorageService.storeFile(file);

        String fileDownloadUrl = "http://localhost:8000/api/memes/downloadFile/" + fileName;

        File newMeme = new File(fileName, legend, userId, fileDownloadUrl, file.getContentType());
        return fileRepository.save(newMeme);
    }

    @PostMapping("/update/{id}")
    public ResponseEntity updateMeme(@RequestParam("file") MultipartFile fileUploaded,
                                     @RequestParam String legend, @RequestParam String userId,
                                     @PathVariable("id") final Integer id) {

        Optional<File> file = fileRepository.findById(id);
        if (file.isPresent()) {
            if (!fileUploaded.isEmpty()) {
                String fileName = fileStorageService.storeFile(fileUploaded);
                String fileDownloadUrl = "http://localhost:8000/api/memes/downloadFile/" + fileName;

                file.get().setFileName(fileName);
                file.get().setDownloadUrl(fileDownloadUrl);
                file.get().setFileType(fileUploaded.getContentType());
            }
            if (legend != null)
                file.get().setLegend(legend);
            if (userId != null)
                file.get().setUserId(userId);

            return new ResponseEntity<>(fileRepository.saveAndFlush(file.get()), HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity deleteById(@PathVariable("id") final Integer id) {
        Optional<File> f = fileRepository.findById(id);
        if (f.isPresent()) {
            fileRepository.deleteById(id);
            fileStorageService.deleteFile(f.get().getFileName());

            return new ResponseEntity(HttpStatus.OK);
        }
        return new ResponseEntity(HttpStatus.NOT_FOUND);
    }

}
