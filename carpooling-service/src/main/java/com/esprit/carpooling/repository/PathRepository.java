package com.esprit.carpooling.repository;

import com.esprit.carpooling.model.Path;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PathRepository extends JpaRepository<Path, Integer> {

}
