package com.esprit.carpooling.controller;

import com.esprit.carpooling.apierror.EntityNotFoundException;
import com.esprit.carpooling.model.Path;
import com.esprit.carpooling.repository.PathRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
public class CarpoolingController {

    @Autowired
    private PathRepository pathRepository;

    @GetMapping("/{id}")
    public Path getById(@PathVariable("id") final Integer id) throws EntityNotFoundException {
        return pathRepository.getOne(id);
    }

    @GetMapping("/all")
    public List<Path> getAll() {
        return pathRepository.findAll();
    }

    @PostMapping("/add")
    public Path add(@RequestBody final @Valid Path path) {
        return pathRepository.save(path);
    }


//    @PostMapping("/delete/{username}")
//    public List<String> delete(@PathVariable("username") final String username) {
//
//        List<Quote> quotes = pathRepository.findByUserName(username);
//        pathRepository.delete(quotes);
//
//        return getQuotesByUserName(username);
//    }
//
//
//    private List<String> getQuotesByUserName(@PathVariable("username") String username) {
//        return pathRepository.findByUserName(username)
//                .stream()
//                .map(Quote::getQuote)
//                .collect(Collectors.toList());
//    }


}
