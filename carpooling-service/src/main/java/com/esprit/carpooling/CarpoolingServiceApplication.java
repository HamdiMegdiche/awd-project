package com.esprit.carpooling;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class CarpoolingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarpoolingServiceApplication.class, args);
    }

}
