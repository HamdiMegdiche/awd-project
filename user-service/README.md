# user-service built with loopback 3 api

```
$ cd main_project_directory/user-service
$ npm install
$ npm start
```

## Project Layout
- `common/models` contains the extended user files. `user.js` contains the logic for sending emails and password reset, while `user.json` contains the model definition.
- `server/boot/authentication.js` enables authentication middleware with the `enableAuth()` method. It's this middleware that finds the access token id string (usually from the query string) and appends entire token instance onto the express request object as `req.accessToken`. From there, you can find the user's ID: `req.accessToken.userId` (used in the `routes.js` file, see directly below).
- `server/boot/routes.js` contains all the routing logic. In this example, we have used [Express](http://expressjs.com/) to configure the routing since each LoopBack app is an extended version of an Express app.
- `server/views`contains all the views (or pages) rendered by Express using the [EJS templating framework](http://www.embeddedjs.com/)
- `server/datasources.json` contains the datasource configurations for email(sendgrid api) datasource and mongoDB(atlascloud remote mongodb database).
- `server/model-config.json` contains the all the model configurations. Here is where we configure the extended user model (lowercase 'u') and the email model. The rest of the models are all built-in LoopBack models.

## How do you register a new user?
1. Create a [form](https://github.com/strongloop/loopback-example-user-management/blob/master/server/views/login.ejs#L21-L36) to gather sign up information
2. Create a [remote hook](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L5-L35) to [send a verification email](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L9-L34)

###### Notes
- Upon execution, [`user.verify`](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L19) sends an email using the provided [options](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L9-L17)
- The verification email is configured to [redirect the user to the `/verified` route](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L15) in our example. For your app, you should configure the redirect to match your use case
- The [options](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L9-L17) are self-explanatory except `type`, `template` and `user`
  - `type` - value must be `email`
  - `template` - the path to the template to use for the verification email
  - `user` - when provided, the information in the object will be used in the verification link email

###### Notes
- We use the LoopBack token middleware to process access tokens. As long as you provide `access_token` in the query string of URL, the access token object will be provided in `req.accessToken` property in your route handler

## How do you perform a password reset for a registered user?
1. Create a [form to gather password reset info](https://github.com/strongloop/loopback-example-user-management/blob/master/server/views/login.ejs#L40-L51)
2. Create an [endpoint to handle the password reset request](https://github.com/strongloop/loopback-example-user-management/blob/master/server/boot/routes.js#L69-L83). Calling `User.resetPassword` ultimately emits a `resetPasswordRequest` event and creates a temporary access token
3. Register an event handler for the `resetPasswordRequest` that sends an email to the registered user. In our example, we provide a [URL](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L54) that redirects the user to a [password reset page authenticated with a temporary access token](https://github.com/strongloop/loopback-example-user-management/blob/master/server/boot/routes.js#L85-L92)
4. Create a [password reset form](https://github.com/strongloop/loopback-example-user-management/blob/master/server/views/password-reset.ejs#L2-L17) for the user to enter and confirm their new password
5. Create an [endpoint to process the password reset](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L79-L87)

- For the `resetPasswordRequest` handler callback, you are provided with an [`info`](https://github.com/strongloop/loopback-example-user-management/blob/master/common/models/user.js#L53) object which contains information related to the user that is requesting the password reset. Note that this example is set up to send an initial email to yourself (the FROM and TO fields are the same). You will eventually want to change the address in the FROM field.
