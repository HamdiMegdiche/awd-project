package com.esprit.lostfound.controller;

import com.esprit.lostfound.apierror.EntityNotFoundException;
import com.esprit.lostfound.model.Item;
import com.esprit.lostfound.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/")
public class LostFoundController {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private JavaMailSender javaMailSender;

    @GetMapping("/{id}")
    public Item getById(@PathVariable("id") final Integer id) throws EntityNotFoundException {
        return itemRepository.getOne(id);
    }

    @GetMapping("/all")
    public List<Item> getAll() {
        return itemRepository.findAll();
    }

    @PostMapping("/addOrUpdate")
    public Item addOrUpdate(@RequestBody final @Valid Item path) {
        return itemRepository.save(path);
    }

    @PostMapping("/found/{id}")
    public Item found(@PathVariable("id") final Integer id, @RequestParam String userId) throws EntityNotFoundException {
        Item found = itemRepository.getOne(id);
        found.setFound(true);
        found.setFoundBy(userId);

        SimpleMailMessage msg = new SimpleMailMessage();
        // get item.user
        // replace this email by item.user email adress
        msg.setTo("montassar.laaribi@gmail.com");

        msg.setSubject(found.getTitle() + " has been found!");
        msg.setText("Your item has been found contact user :" + userId);

        javaMailSender.send(msg);
        return itemRepository.save(found);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity delete(@PathVariable("id") final Integer id) throws EntityNotFoundException {
        Item item = itemRepository.getOne(id);
        itemRepository.delete(item);
        return ResponseEntity.ok("Deleted");
    }

}
