package com.esprit.lostfound;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient
public class LostfoundServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(LostfoundServiceApplication.class, args);
    }

}
