import React, {useState, useEffect} from "react";
import CssBaseline from "@material-ui/core/CssBaseline";
import Container from "@material-ui/core/Container";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";
import Slider from "@material-ui/core/Slider";

import axios from "./../api";
import {withStyles} from "@material-ui/core/styles";
import styles from "./../styles/styles";
import Typography from "@material-ui/core/Typography";
import FilmList from "./HouseList";
import Pagination from "./Pagination";

const Search = ({classes, props}) => {
  const [films, setFilms] = useState([]);
  const [searchedFilms, setSearchedFilms] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);
  const [filmsPerPage] = useState(9);
  const [searchTerm, setSearchTerm] = useState("");
  const [value, setValue] = useState([0, 1000]);

  useEffect(() => {
    const fetchFilms = async () => {
      setLoading(true);
      const res = await axios.get("/all");
      console.log(res.data);
      setFilms(res.data);
      setLoading(false);
    };

    fetchFilms();
  }, []);

  //pagination
  const indexOfLastFilm = currentPage * filmsPerPage;
  const indexOfFirstFilm = indexOfLastFilm - filmsPerPage;
  const currentFilms = !searchTerm
    ? films.slice(indexOfFirstFilm, indexOfLastFilm)
    : searchedFilms.slice(indexOfFirstFilm, indexOfLastFilm);

  // Change page
  const paginate = pageNumber => setCurrentPage(pageNumber);

  function handleChange(event) {
    setSearchTerm(event.target.value);

    if (searchTerm) {
      console.log("searchTerm:", searchTerm);
      let result = films.filter(film => {
        return film.location.indexOf(event.target.value) !== -1;
      });
      console.log("result:", result);

      setSearchedFilms(result);
    }
  }


function valuetext(value) {
  return `${value}DT`;
  }
  
  function handleChangePrice(event, newValue) {
    setValue(newValue);
    axios.get("/search/" + newValue[0] + "/" + newValue[1]).then((res) => {
      setFilms(res.data);
     });

    
  }
  const Allfilms = searchedFilms.length ? searchedFilms.length : films.length;

  return (
    <React.Fragment>
      <CssBaseline />
      <main className={classes.mainContent}>
        <div className={classes.heroContent}>
          <Typography align="center" className={classes.title}>
            Trouver un film à voir facilement et rapidement !
          </Typography>
          <Container maxWidth="sm" className={classes.container}>
            <div className={classes.search}>
              <div className={classes.searchIcon}>
                <SearchIcon />
              </div>
              <InputBase
                placeholder="Search…"
                classes={{
                  root: classes.inputRoot,
                  input: classes.inputInput
                }}
                name="searchTerm"
                value={searchTerm}
                onChange={event => handleChange(event)}
                inputProps={{"aria-label": "search"}}
              />
            </div>
            <Typography id="range-slider" gutterBottom>
              Price range
            </Typography>
            <Slider
              value={value}
              onChange={handleChangePrice}
              valueLabelDisplay="auto"
              aria-labelledby="range-slider"
              getAriaValueText={valuetext}
              min={0}
              max={1000}
              step={10}
            />
          </Container>
        </div>

        <FilmList
          films={currentFilms}
          searchTerm={searchTerm}
          loading={loading}
        />
        <Pagination
          filmsPerPage={filmsPerPage}
          totalFilms={Allfilms}
          paginate={paginate}
          currentPage={currentPage}
        />
      </main>
    </React.Fragment>
  );
};

export default withStyles(styles)(Search);
