import React from 'react'
import Typography from '@material-ui/core/Typography';
import { withStyles } from "@material-ui/core/styles";
import styles from "../styles/styles";


const Footer = (props) => {

  const { classes } = props;
  return (
    <footer className={classes.footer}>

      <Typography variant="body1" align="center" component="p">
        Application web distribué
                </Typography>
      <Typography variant="body2" align="center" >
        {'Copyright © '}
        Hexagone 2019.
      </Typography>


    </footer>

  );

}

export default withStyles(styles)(Footer);