import React, { Component } from "react";
import { withStyles } from "@material-ui/core/styles";
import styles from "../styles/styles";
import { Typography, Button, Paper } from "@material-ui/core";
import axios from "../api";

class HouseDetails extends Component {
  constructor(props) {
    super();
    this.state = {
      film: {},
      cast: [],
      loaded: false,
    };
  }

  componentDidMount() {
    console.log(this.props)
    // &embed=cast permet de donner plus d'informations sur le film
    axios.get('/'+this.props.match.params.id).then(result => {
      this.setState({
        film: result.data,
        loaded: true,
      })

    }).catch(err => {
      console.log('err:', err)
    })
  }

  handleReturn = () => {
    this.props.history.push('/');
  }
  render() {
    const { film } = this.state;
    const { classes } = this.props;
    return (
      <React.Fragment>
        <main className={classes.layout}>
          <Paper className={classes.paper}>
            <Typography className={classes.detailsTitle} variant="h4">
              {film.location}
            </Typography>
            <Typography variant="subtitle1">
              <b>Description</b>:{" "}
            </Typography>
            <Typography className={classes.detailsText} >
              {film.description}
            </Typography>
            <Typography>
              <b>PRICE : </b>
              {film.price } DT
            </Typography>
            <Typography>
              <b>PERDID : </b>
              {film.period} MONTHS
            </Typography>
            <Typography>
              <b>Diffusé le : </b>
              {film.createDateTime ? film.createDateTime : 'Non disponible..'}
            </Typography>

            <div className={classes.detailsButton}>
              <Button
                variant="contained"
                color="primary"
                className={classes.retourButton}
                onClick={this.handleReturn}
              >
                Retour
              </Button>
            </div>
          </Paper>
        </main>
      </React.Fragment>
    );
  }
}

export default withStyles(styles)(HouseDetails);
