import React from 'react';
import Card from '@material-ui/core/Card';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import { Button } from "@material-ui/core";
import {withStyles} from "@material-ui/core/styles";
import styles from "../styles/styles";
import { withRouter} from 'react-router-dom'


const HouseCard = (props) => {

  const { classes, film } = props;
  
  const handleClick = (id) => { 
    props.history.push('/details/'+ id);
  }


  return (
      <Grid  item xs={12} sm={6} md={4} >
      <Card className={classes.card}>
      
        <CardContent className={classes.cardContent}>
        <Typography gutterBottom variant="h2" component="h2" align="center">
                      {film.location}
          </Typography>
          <Typography gutterBottom variant="h5" component="h2" align="center">
                      {film.price} DT
          </Typography>
          <Typography gutterBottom variant="h5" component="h2" align="center">
                      {film.period} MONTHS
          </Typography>

          <Typography gutterBottom variant="h5" component="h2" align="center">
                      {film.description}
          </Typography>
          <Typography gutterBottom variant="h5" component="h2" align="center">
                      {film.nbrPerson} Inhabitants
          </Typography>

        </CardContent>
              <CardActions className={classes.cardActions}>
          <Button  color="primary" className={classes.cardButton} onClick={() => handleClick(film.id)}>
            Voir détails
          </Button>
        </CardActions>
      </Card>
    </Grid>
  );
    
}

export default withRouter(withStyles(styles)(HouseCard));