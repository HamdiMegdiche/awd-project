package com.esprit.flatsharing.controller;


import com.esprit.flatsharing.apierror.EntityNotFoundException;
import com.esprit.flatsharing.model.House;
import com.esprit.flatsharing.repository.HouseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/")
public class HouseController {


    private final HouseRepository houseRepository;

    @Autowired
    public HouseController(HouseRepository houseRepository) {
        this.houseRepository = houseRepository;

    }

    @GetMapping(value = "/{id}",produces = MediaType.APPLICATION_JSON_VALUE)
    public Optional<House> getById(@PathVariable("id") final Integer id) throws EntityNotFoundException {
         return houseRepository.findById(id);
    }

    @GetMapping(value = "/search/{location}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<House> getByLocation(@PathVariable("location") final String location) throws EntityNotFoundException {
        return  houseRepository.houseByName(location);
    }

    @GetMapping(value = "/search/{minPrice}/{maxPrice}",produces = MediaType.APPLICATION_JSON_VALUE)
    public List<House> getByPrice(@PathVariable("minPrice") final float minPrice,@PathVariable("maxPrice") final float maxPrice) throws EntityNotFoundException {
        return  houseRepository.houseByPrice(minPrice,maxPrice);
    }

    @GetMapping("/all")
    public List<House> getAll() {
        return houseRepository.findAll();
    }

    @PostMapping("/add")
    public House add(@RequestBody final @Valid House house) {
        return houseRepository.save(house);
    }

    @PutMapping("/update/{id}")
    public House update(@PathVariable("id") final Integer id, @RequestBody final @Valid House newHouse) {

        Optional<House> updatedHouse = houseRepository.findById(id);

        if(updatedHouse.isPresent()){
            House houseFound = updatedHouse.get();
            houseFound.setDescription(newHouse.getDescription());
            houseFound.setLocation(newHouse.getLocation());
            houseFound.setNbrPerson(newHouse.getNbrPerson());
            houseFound.setPeriod(newHouse.getPeriod());
            houseFound.setPrice(newHouse.getPrice());
            return houseRepository.save(houseFound);
        }else{
            return newHouse;
        }
    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public void deleteHouse(@PathVariable("id") final Integer id){
         houseRepository.deleteById(id);
    }

}
