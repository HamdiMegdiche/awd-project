package com.esprit.flatsharing.repository;

import com.esprit.flatsharing.model.House;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;


public interface HouseRepository extends JpaRepository<House, Integer> {
    @Query("select h from House h where h.location LIKE %:location% ")
    public List<House> houseByName(@Param("location") String location);

    @Query("select h from House h where h.price BETWEEN :minPrice AND :maxPrice ")
    public List<House> houseByPrice(@Param("minPrice") float minPrice,@Param("maxPrice") float maxPrice);
}
